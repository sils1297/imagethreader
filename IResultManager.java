/**
 * Copyright (C) 2013 Lasse R. A. Schuirmann
 * 
 * This file is part of ImageThreader.
 * 
 * ImageThreader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * ImageThreader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with ImageThreader.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.tuhh.ti5.swp.tm.imagethreader;

import java.awt.Point;

/**
 * Interface for a management class that adds up the results of every single
 * pixel to the desired result.
 * 
 * @param <S>
 *            Type of a single result.
 */
public interface IResultManager<S> {
	/**
	 * Adds a part of the result calculation to the result.
	 * 
	 * @param result
	 *            Contains the information.
	 * @param point
	 *            Corresponding pixel.
	 */
	void addPart(S result, Point point);
}

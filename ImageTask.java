/**
 * Copyright (C) 2013 Lasse R. A. Schuirmann
 * 
 * This file is part of ImageThreader.
 * 
 * ImageThreader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * ImageThreader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with ImageThreader.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.tuhh.ti5.swp.tm.imagethreader;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.concurrent.CountDownLatch;

/**
 * Contains all the necessary functions for parallelization of an image task.
 * 
 * You just have to add the functionality in a derivation!
 *
 * @param <S> Type of one specific result.
 */
public abstract class ImageTask<S> implements Runnable {
	private IResultManager<S> mResult;
	private IRaster mRaster;
	private CountDownLatch mLatch;

	private MODE mMode;
	private Rectangle mRegion;
	private int mBegin;
	private int mDistance;
	
	/**
	 * Calculates the result for one pixel. The returned value will be passed to addPart of the result manager.
	 * 
	 * @param image The image to process.
	 * @param pixel The pixel to process.
	 * @return A single result.
	 */
	protected abstract S calcPixel(IRaster image, Point pixel);
	
	/**
	 * Will be called to execute one task.
	 * 
	 * This function assumes that all parameters are set before since it shall only
	 * be invoked by an ImageManager class.
	 */
	public void run() {
		try {
			switch (mMode) {
				case DISTRIBUTED_MODE:
					distributedPixelCalc();
					break;
				case REGION_MODE:
					regionPixelCalc();
					break;
				default:
					throw new UnsupportedOperationException("Mode was set to an "
							+ "unsupported state. You might want to implement a new mode.");
			}
		} finally {
        	mLatch.countDown();
        }
	}
	
	/**
	 * Performs the task using splitting to regions.
	 */
	private void regionPixelCalc() {
		for (int col = mRegion.x; col < mRegion.x + mRegion.width; col++) {
			for (int row = mRegion.y; row < mRegion.y + mRegion.height; row++) {
				Point point = new Point(col, row);
				mResult.addPart(calcPixel(mRaster, point), point);
			}
		}
	}
	
	/**
	 * Performs the task with distributed pixels.
	 */
	private void distributedPixelCalc() {
		for (int row = 0; row < mRaster.getHeight(); row++) {
			for (int col = mBegin; col < mRaster.getWidth(); col += mDistance) {
				Point point = new Point(col, row);
				mResult.addPart(calcPixel(mRaster, point), point);
			}
		}
	}
	
	/**
	 * Sets the mode to region mode and sets the region.
	 * 
	 * @param region The region.
	 */
	public final void setRegion(Rectangle region) {
		mMode = MODE.REGION_MODE;
		this.mRegion = region;
	}
	
	/**
	 * Sets the mode to distributed mode and sets the necessary parameters.
	 * 
	 * @param begin First pixel of a line to calculate.
	 * @param distance Distance between two calculate-pixels. (reasonable: thread count)
	 */
	public final void setRegion(int begin, int distance) {
		mMode = MODE.DISTRIBUTED_MODE;
		this.mBegin = begin;
		this.mDistance = distance;
	}
	
	/**
	 * Setter for the raster.
	 * 
	 * @param image IRaster
	 */
	public final void setRaster(IRaster image) {
		this.mRaster = image;
	}
	
	/**
	 * Setter for the result.
	 * 
	 * @param result The result.
	 */
	public final void setResult(IResultManager<S> result) {
		this.mResult = result;
	}
	
	/**
	 * Setter for the latch.
	 * 
	 * @param latch latch
	 */
	public final void setLatch(CountDownLatch latch) {
		mLatch = latch;
	}
}

/**
 * Copyright (C) 2013 Lasse R. A. Schuirmann
 * 
 * This file is part of ImageThreader.
 * 
 * ImageThreader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * ImageThreader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with ImageThreader.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.tuhh.ti5.swp.tm.imagethreader;

/**
 * A raster object. This may e.g. be a WritableRaster or any other type that contains the
 * information about all the pixels and the height and width.
 */
public interface IRaster {
	/**
	 * Gets the width of the raster.
	 * 
	 * @return width
	 */
	int getWidth();

	/**
	 * Gets the height of the raster.
	 * 
	 * @return height
	 */
	int getHeight();
}

/**
 * Copyright (C) 2013 Lasse R. A. Schuirmann
 * 
 * This file is part of ImageThreader.
 * 
 * ImageThreader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * ImageThreader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with ImageThreader.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.tuhh.ti5.swp.tm.imagethreader;

/**
 * Describes which mode is used to split the image.
 * 
 * REGION_MODE:
 * 		Partitions the image into vertical regions.
 * 		+----+----+----+
 * 		|    |    |    |
 * 		|    |    |    |
 * 		|    |    |    |
 * 		|    |    |    |
 * 		+----+----+----+
 * 		Pic. 1, Regions of 3 tasks (divided by |)
 * 
 * DISTRIBUTED_MODE:
 * 		Partitions the image more spread. One of n tasks will get the m
 * 		(0 <= m <= n) pixel and every n next pixel.
 * 		+--------------+
 * 		| +    +    +  |
 * 		| +    +    +  |
 * 		| +    +    +  |
 * 		| +    +    +  |
 * 		+--------------+
 * 		Pic. 2, Pixels of one of 5 tasks (marked with +)
 * 
 * Feel free to add other modes if it makes sense - be sure to implement them in ImageManager and ImageTask.
 * 
 * @author Lasse Schuirmann
 */

public enum MODE {
	REGION_MODE,
	DISTRIBUTED_MODE;
}

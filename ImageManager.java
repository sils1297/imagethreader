/**
 * Copyright (C) 2013 Lasse R. A. Schuirmann
 * 
 * This file is part of ImageThreader.
 * 
 * ImageThreader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * ImageThreader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with ImageThreader.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.tuhh.ti5.swp.tm.imagethreader;

import java.awt.Rectangle;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;

/**
 * Manages images with multiple threads.
 * 
 * @param <T>
 *            Type of the task.
 * @param <S>
 *            Single result.
 */

public class ImageManager<T extends ImageTask<S>, S> {
	private Class<T> mTask;

	private IResultManager<S> mResult;
	private IRaster mRaster;
	private int mTaskCount;

	private ExecutorService mExecutor;
	private CountDownLatch mLatch;

	private boolean mResultIsSet = false;
	private boolean mImageIsSet = false;
	private boolean mFinished = false;

	private MODE mMode = MODE.REGION_MODE;

	/**
	 * Default constructor.
	 * 
	 * @param task
	 *            Needed to create instances of type T.
	 * @param taskCount
	 *            Amount of tasks to be created.
	 * @param threadPool
	 *            ThreadPool used
	 */
	public ImageManager(Class<T> task, ExecutorService threadPool, int taskCount) {
		if (task == null || threadPool == null) {
			throw new NullPointerException();
		}
		if (taskCount <= 0) {
			throw new IllegalArgumentException(
					"Argument 'taskCount' has to be larger or equal to zero.");
		}

		mTask = task;
		mExecutor = threadPool;
		mTaskCount = taskCount;
		mLatch = new CountDownLatch(taskCount);
	}

	/**
	 * Executes threads and does the calculation. Blocks until the calculation is finished.
	 * 
	 * @return The result manager. (May also be achieved via getResult)
	 * @throws IllegalAccessException
	 *             Occurs if an error occurs while creating a new instance of a task.
	 * @throws InstantiationException
	 *             Occurs if T is an abstract class or interface.
	 * @throws InterruptedException
	 *             Occurs if thread is interrupted.
	 */
	public final IResultManager<S> call() throws InstantiationException, IllegalAccessException,
			InterruptedException {
		mFinished = false;

		if (!(mResultIsSet && mImageIsSet)) {
			throw new IllegalStateException("'setResultManager(IResultManager<S> resultManager)'"
					+ " and 'setImage(IRaster image)' have to be invoked before 'call()'.");
		}

		executeTasks();
		mLatch.await();

		mFinished = true;

		return mResult;
	}

	/**
	 * Creates and executes the tasks.
	 * 
	 * @throws InstantiationException
	 *             Occurs if T is an abstract class or interface.
	 * @throws IllegalAccessException
	 *             Occurs if an error occurs while creating a new instance of a task.
	 */
	private void executeTasks() throws InstantiationException, IllegalAccessException {
		for (int i = 0; i < mTaskCount; i++) {
			T task = createTask();
			initTask(task, i);
			mExecutor.execute(task);
		}
	}

	/**
	 * Getter for the result manager.
	 * 
	 * @return the result manager.
	 */
	public IResultManager<S> getResult() {
		if (!mFinished) {
			throw new IllegalStateException("Not all tasks are finished yet.");
		}
		return mResult;
	}

	/**
	 * Creates an instance of a task and configures it.
	 * 
	 * Overwrite this method to create a task with additional data.
	 * 
	 * @return the instance.
	 * @throws InstantiationException
	 *             Occurs if T is an abstract class or interface.
	 * @throws IllegalAccessException
	 *             Occurs if an error occurs while creating a new instance of a task.
	 */
	protected T createTask() throws InstantiationException, IllegalAccessException {
		T task = mTask.newInstance();
		if (task == null) {
			throw new NullPointerException("");
		}
		return task;
	}

	/**
	 * Initializes the given task.
	 * 
	 * @param task
	 *            Task to initialize
	 * @param i
	 *            Task number (0 to n)
	 */
	private void initTask(T task, int i) {
		if (task == null) {
			throw new NullPointerException();
		}
		if (i < 0 || i >= mTaskCount) {
			throw new IllegalArgumentException(
					"Argument 'i' has to be a number in range [0, taskCount).");
		}
		task.setRaster(mRaster);
		task.setResult(mResult);
		task.setLatch(mLatch);

		setTaskMode(task, i);
	}

	/**
	 * Sets the MODE and the according parameters.
	 * 
	 * @param task
	 *            Reference to the task.
	 * @param i
	 *            Task id.
	 */
	private void setTaskMode(T task, int i) {
		switch (mMode) {
		case DISTRIBUTED_MODE:
			task.setRegion(i, mTaskCount);
			break;
		case REGION_MODE:
			int offs = mRaster.getWidth() / mTaskCount;
			if (i == mTaskCount - 1) {
				task.setRegion(new Rectangle(i * offs, 0, mRaster.getWidth() - (i * offs), mRaster
						.getHeight()));
			} else {
				task.setRegion(new Rectangle(i * offs, 0, offs, mRaster.getHeight()));
			}
			break;
		default:
			throw new UnsupportedOperationException("Mode was set to an "
					+ "unsupported state. You might want to implement a new mode "
					+ "in ImageManager.java, void setTaskMode.");
		}
	}

	/**
	 * Setter for the result manager.
	 * 
	 * @param resultManager
	 *            the result manager
	 */
	public void setResultManager(IResultManager<S> resultManager) {
		if (resultManager == null) {
			throw new NullPointerException();
		}
		this.mResult = resultManager;
		mResultIsSet = true;
	}

	/**
	 * Setter for the raster-like object.
	 * 
	 * @param image
	 *            The image which will be processed. Type: IRaster.
	 */
	public void setImage(IRaster image) {
		if (image == null) {
			throw new NullPointerException();
		}
		this.mRaster = image;
		mImageIsSet = true;
	}

	/**
	 * Setter for the mode.
	 * 
	 * See MODE.java for more information about modes.
	 * 
	 * @param mode
	 *            MODE type.
	 */
	public void setMode(MODE mode) {
		mMode = mode;
	}
}